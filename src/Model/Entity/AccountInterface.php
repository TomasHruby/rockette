<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Entity;

interface AccountInterface
{

    const STATUS_DELETED = -4;
    const STATUS_BANNED = -3;
    const STATUS_EXPIRED = -2;
    const STATUS_PENDING = -1;
    const STATUS_CREATED = 0; //new
    const STATUS_ALLOWED = 1;

    /**
     * @return bool
     */
    public function isStatusOk(): bool;

}
