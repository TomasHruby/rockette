<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Entity;

/**
 * @property-read int $id m:schemaPrimary
 * @property      string $name m:schemaType(varchar:255) m:schemaComment(Name of organization)
 * @property      string $slug m:schemaType(varchar:255) m:schemaComment(Slug used for url)
 * @property      string|null $contactEmail m:schemaType(varchar:255)
 * @property      string|null $contactPhone m:schemaType(varchar:31)
 * @property      string|null $website m:schemaType(varchar:255)
 * @property      string|null $regNumber m:schemaType(varchar:31)
 * @property      string|null $vatNumber m:schemaType(varchar:31)
 * @property      array|null $metadata m:passThru(jsonDecode|jsonEncode) m:schemaType(json)
 * @property      int|null $licence m:enum(self::LICENCE_*) m:schemaType(int) m:default(0) Licence or subscription plan
 * @property      int|null $timezone m:schemaType(varchar:63)
 * @property      DateTime|null $createDate m:schemaType(DateTime)
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class Organization
 * @package Rockette\Model\Entity
 */
final class Organization extends SuperEntity
{

}
