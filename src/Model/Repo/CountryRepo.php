<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Repo;

/**
 * @method \Rockette\Model\Entity\Country getSingle($id)
 * @method \Rockette\Model\Entity\Country[] getMultiple
 *
 * @deprecated ONLY EXAMPLE
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class CountryRepo
 * @package Rockette\Model\Repo
 */
final class CountryRepo extends SuperRepo
{

}
