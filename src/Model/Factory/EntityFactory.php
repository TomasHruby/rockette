<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Model\Factory;

abstract class EntityFactory extends \LeanMapper\DefaultEntityFactory
{

    /**
     * @var \Nette\DI\Container
     */
    protected $container;

    /**
     * @var \Rockette\Model\Mapper\MapperInterface
     */
    protected $mapper;

    /**
     * @param \Nette\DI\Container $container
     * @param \Rockette\Model\Mapper\MapperInterface   $mapper
     */
    public function __construct(\Nette\DI\Container $container, \Rockette\Model\Mapper\MapperInterface $mapper) {
        $this->container = $container;
        $this->mapper = $mapper;
    }

    /**
     * @param  string                     $entityClass
     * @param  \LeanMapper\Row|\Traversable|array|null $arg
     * @return mixed
     */
    public function createEntity(string $entityClass, $arg = NULL): \Rockette\Model\Entity\SuperEntity {
        $entity = $this->container->createInstance($entityClass, [$arg]);
        $this->pushDependencies($entity);
        return $entity;
    }

    /**
     * Call inject* methods on entity
     *
     * @param \Rockette\Model\Entity\SuperEntity $entity
     */
    public function pushDependencies(\Rockette\Model\Entity\SuperEntity $entity) {
        $this->container->callInjects($entity);
    }

    /**
     * Return entities in array indexed by primary key
     *
     * @param \Rockette\Model\Entity\SuperEntity[] $entities
     * @return \Rockette\Model\Entity\SuperEntity[] Indexed by PK
     */
    public function createCollection(array $entities) {

        $result = [];

        foreach ($entities as $entity) {
            $entityClass = get_class($entity);
            $table = $this->mapper->getTable($entityClass);
            $primaryKey = $this->mapper->getPrimaryKey($table);
            $primaryKeyField = $this->mapper->getEntityField($table, $primaryKey);

            if (!isset($result[$entity->$primaryKeyField])) {
                $result[$entity->$primaryKeyField] = $entity;
            } else {
                throw new \LeanMapper\Exception\InvalidStateException("Entity collection collision. Fetched more entities $entityClass with the same primary key!");
            }
        }

        return $result;
    }

}
