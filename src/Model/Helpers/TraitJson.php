<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Rockette\Model\Helpers;

use Nette\Utils\Json;
use Nette\Utils\JsonException;

trait TraitJson
{

    /**
     * Decodes the input value as JSON into an associative PHP array.
     * Can return NULL if the decoding process returns NULL.
     *
     * @param  string|null $value
     * @return array|null
     * @throws JsonException
     */
    protected function jsonDecode($value) {
        if ($value === NULL) {
            return NULL;
        }

        return Json::decode($value, Json::FORCE_ARRAY);
    }

    /**
     * Attempts to encode the input associative array value as string.
     * Accepts NULL values. If the value is NULL, the resulting string is also NULL.
     *
     * @param  array|null $value
     * @return string|null
     * @throws JsonException
     */
    protected function jsonEncode($value) {
        if ($value === NULL) {
            return NULL;
        }
        return Json::encode($value);
    }

}

