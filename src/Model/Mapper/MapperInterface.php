<?php
declare(strict_types=1);

namespace Rockette\Model\Mapper;

/**
 * @author Tomáš Hrubý
 */
interface MapperInterface extends \LeanMapper\IMapper
{

}
