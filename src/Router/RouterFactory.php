<?php

declare(strict_types=1);

namespace Rockette\Router;

abstract class RouterFactory
{
    use \Nette\StaticClass;

    public static function createRouter(): \Nette\Application\Routers\RouteList {

        $router = new \Nette\Application\Routers\RouteList;
        $router[] = self::getApiRouteList();
        $router[] = self::getAdminRouteList();
        $router[] = self::getFrontRouteList();
        $router[] = self::getDevRouteList();
        return $router;
    }

    protected static function getApiRouteList() {
        $router = new \Nette\Application\Routers\RouteList('Api');
        $router->addRoute('api', [ 'presenter' => 'Root', 'action' => 'default']);
        $router->addRoute('api/<presenter>[/<action>][/<id>][/<par1>]', [    'presenter' => 'Dashboard', 'action' => 'default' ]);
        return $router;
    }

    protected static function getFrontRouteList() {
        $router = new \Nette\Application\Routers\RouteList('Front');
        $router->addRoute('<presenter>/<action>[/<id>]', 'Homepage:default');
        return $router;
    }

    protected static function getAdminRouteList() {
        $router = new \Nette\Application\Routers\RouteList('Admin');
        $router->addRoute('admin', [ 'presenter' => 'Dashboard', 'action' => 'default']);
        $router->addRoute('admin/<presenter>[/<action>][/<id>][/<par1>]', [    'presenter' => 'Dashboard', 'action' => 'default' ]);
        return $router;
    }

    protected static function getDevRouteList() {
        $router = new \Nette\Application\Routers\RouteList('Dev');
        $router->addRoute('dev', [ 'presenter' => 'Dashboard', 'action' => 'default']);
        $router->addRoute('dev/<presenter>[/<action>][/<id>][/<par1>]', [    'presenter' => 'Dashboard', 'action' => 'default' ]);
        return $router;
    }

}
