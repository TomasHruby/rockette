<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Security;

class BasicIdentity extends \Nette\Security\SimpleIdentity
{

    public function __construct(\Rockette\Model\Entity\AccountInterface $account) {

        $data = [
            'username' => $account->username,
            'firstname' => $account->firstname,
            'midname' => $account->midname,
            'surname' => $account->surname,
        ];

        $roles = $this->getRoles();

        parent::__construct($account->id, $roles, $data);
    }

    public function getRoles(): array {
        return $this->roles;
    }

}
