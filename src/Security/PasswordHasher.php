<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Security;

/**
 * Own implementation of Nette Passwords, methods could be changed or updated
 * @author  Tom Hruby
 * https://tomashruby.com
 * Class PasswordHasher
 * @package Rockette\Security
 */
class PasswordHasher
{
    /**
     * @var \Nette\Security\Passwords
     */
    protected $passwords;

    /**
     * @param \Nette\Security\Passwords $passwords
     */
    public function __construct(\Nette\Security\Passwords $passwords) {
        $this->passwords = $passwords;
    }

    /**
     * Computes password´s hash. The result contains the algorithm ID and its settings, cryptographical salt and the hash itself.
     */
    public function hash(string $password): string {
        return $this->passwords->hash($password);
    }

    /**
     * Finds out, whether the given password matches the given hash.
     */
    public function verify(string $password, string $hash): bool {
        return $this->passwords->verify($password, $hash);
    }

    /**
     * Finds out if the hash matches the options given in constructor.
     */
    public function needsRehash(string $hash): bool {
        return $this->passwords->needsRehash($hash);
    }

}
