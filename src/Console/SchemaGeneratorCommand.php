<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */
declare(strict_types=1);

namespace Rockette\Console;

use CzProject\Logger\OutputLogger;
use Inlm\SchemaGenerator\Adapters\NeonAdapter;
use Inlm\SchemaGenerator\Database;
use Inlm\SchemaGenerator\Dumpers\SqlDumper;
use Inlm\SchemaGenerator\Extractors\LeanMapperExtractor;
use Inlm\SchemaGenerator\SchemaGenerator;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class SchemaGeneratorCommand extends Command
{

    const IGNORED_TABLES = [
        'migrations',
    ];

    /**
     * Directories with entities to generate schema
     * Also abstract entities must be included
     */
    const ENTITY_DIRECTORIES = [
        ROOT_DIR . '/app/Model/Entity/',
        ROOT_DIR . '/vendor/tomashruby/rockette/src/Model/Entity/',
    ];

    const ROOT_DIR = ROOT_DIR;

    const ARGUMENT_JOB = 'job';

    const ARGUMENT_DEMO = 'demo';

    /**
     * @var \Rockette\Service\ConnectionServiceInterface
     */
    protected $connectionService;

    /**
     * @var \Rockette\Model\Mapper\MapperInterface
     */
    protected $mapper;

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'schema-generator';

    public function __construct(
        \Rockette\Service\ConnectionServiceInterface $connectionService,
        \Rockette\Model\Mapper\MapperInterface $mapper
    ) {
        parent::__construct();
        $this->connectionService = $connectionService;
        $this->mapper = $mapper;
    }

    protected function configure(): void {
        $this->setDescription('ILNM Schema generator');
        $info = 'job:' . PHP_EOL;
        $info .= ' - create: generate migrations/schema/schema.neon and create new sql migration (in migrations/structures)' . PHP_EOL;
        $info .= ' - update: run migrations/structures/ sql scripts' . PHP_EOL;
        $this->addArgument(self::ARGUMENT_JOB, InputArgument::REQUIRED, $info);
        $this->addArgument(self::ARGUMENT_DEMO, InputArgument::OPTIONAL, 'demo:\n1 - for demo without affecting schema.neon or database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {

        //Init setup
        $extractor = new LeanMapperExtractor(static::ENTITY_DIRECTORIES, $this->mapper);
        $typeOfWork = $input->getArgument('job');
        $isDemo = $input->getArgument('demo');
        try {
            switch ($typeOfWork) {
            case 'create':
                $adapter = new NeonAdapter(self::ROOT_DIR . '/migrations/schema/schema.neon');
                $dumper = new SqlDumper(self::ROOT_DIR . '/migrations/structures/');
                break;
            case 'update':
                throw new InvalidArgumentException('This job is not implemented yet. Please use migrations:continue to update database with alters, drops and inserts.');
            default:
                throw new InvalidArgumentException('Unknown typeOfWork parameter.');
            }

            $output->writeln('Starting job...');

            //continue
            $dumper->enablePositionChanges();
            $logger = new OutputLogger();

            //generate
            $generator = new SchemaGenerator($extractor, $adapter, $dumper, $logger, Database::MYSQL);
            $generator->setOption('COLLATE', 'utf8mb4_general_ci');
            $generator->setOption('ENGINE', 'InnoDB');
            $generator->setOption('CHARACTER SET', 'utf8mb4');
            if ($isDemo) {
                $generator->setTestMode(TRUE);
            }
            $generator->generate();

        } catch (\Throwable $e) {
            $output->writeln('Error while doing job: ' . $e->getMessage());
            return Command::FAILURE;
        }
        $output->writeln('End of migrations job.');
        return Command::SUCCESS;
    }

}
